int countEnters= 0;
int countExits = 0;
int peopleInside = 0;

unsigned long doorSensorTriggerTime = 0;
unsigned long irSensorTriggerTime = 0;

// D4 is the pin for magnetic switch on the door. 
// When the door is closed, it would be high. 
// If door is open, it would be low. 
void doorTrigger() {
    // Debounce the sensor. 
    doorSensorTriggerTime = millis();
    Serial.println("Door triggered");

}

void irTrigger() {
    irSensorTriggerTime = millis();
    Serial.println("IR triggered");
}

void setup() {
    Serial.begin(9600);
    pinMode(D4, INPUT);
    pinMode(D5, INPUT);
    attachInterrupt(D4, doorTrigger, RISING);
    attachInterrupt(D5, irTrigger, RISING);
    // Associate the people variable with the Particle Cloud API. 
    Particle.variable("peopleInside", peopleInside);
}

void loop() {
    // IR sensor first, then door -> a person is leaving the building
    if (doorSensorTriggerTime - irSensorTriggerTime > 0 && doorSensorTriggerTime - irSensorTriggerTime < 5 * 1000)
        countExits++;
    // Door sensor first, then IR -> a persion is entering the building. 
    else if (irSensorTriggerTime - doorSensorTriggerTime > 0 &&  irSensorTriggerTime - doorSensorTriggerTime < 5 * 1000)
        countEnters++;
    if (countEnters - countExits >= 0)
        peopleInside = countEnters - countExits;
    else 
        peopleInside = 0;
    delay(20);
}